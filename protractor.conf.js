exports.config = {
  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  capabilities: {
  	'browserName': 'chrome'
  },
  specs: ['./src/client/tests-e2e/**/*.js'],
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    isVerbose: false,
    includeStackTrace: true,
    defaultTimeoutInterval: 30000
  }
};