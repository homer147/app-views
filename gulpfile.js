var gulp = require('gulp');
var args = require('yargs').argv;
var browserSync = require('browser-sync');
var config = require('./gulp.config')();
var del = require('del');
var port = process.env.PORT || config.defaultPort;
var protractor = require('gulp-protractor').protractor;
var Server = require('karma').Server;
var webdriver_standalone = require('gulp-protractor').webdriver_standalone;
var webdriver_update = require('gulp-protractor').webdriver_update;
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('wiredep', function() {
	log('Wire up the bower css, lib js and app js into the html');
	var options = config.getWiredepDefaultOptions();
	var wiredep = require('wiredep').stream;

	return gulp
		.src(config.index)
		.pipe(wiredep(options))
		.pipe($.inject(gulp.src(config.js)))
		.pipe(gulp.dest(config.client));
});

gulp.task('inject', ['wiredep', 'styles', 'templatecache'], function() {
	log('Wire up the app css into the html and call wiredep');
	var options = config.getWiredepDefaultOptions();
	var wiredep = require('wiredep').stream;

	return gulp
		.src(config.index)
		.pipe($.inject(gulp.src(config.css)))
		.pipe(gulp.dest(config.client));
});

gulp.task('styles', ['clean-styles'], function() {
	log('Compiling Sass --> CSS');
	return gulp
		.src(config.sass)
		.pipe($.plumber())
		.pipe($.sass())
		.pipe($.autoprefixer({browsers: ['last 2 versions', '> 5%']}))
		.pipe(gulp.dest(config.temp));
});

gulp.task('sass-watcher', function() {
	gulp.watch([config.sassWatch], ['styles']);
});

gulp.task('templatecache', ['clean-code'], function() {
	log('Creating AngularJS $templateCache');
	return gulp
		.src(config.htmltemplates)
		.pipe($.htmlmin({collapseWhitespace: true}))
		.pipe($.angularTemplatecache(
			config.templateCache.file,
			config.templateCache.options
		))
		.pipe(gulp.dest(config.temp));
});

gulp.task('fonts', ['clean-fonts'], function() {
	log('Copying fonts');

	return gulp
		.src(config.fonts)
		.pipe(gulp.dest(config.public + 'fonts'));
});

gulp.task('images', ['clean-images'], function() {
	log('Copying and minifing the images');

	return gulp
		.src(config.images)
		.pipe($.imagemin())
		.pipe(gulp.dest(config.public + 'images'));
});

gulp.task('optimise', ['inject', 'images', 'fonts'], function() {
	log('Optimising the javascript, css and html');

	var templateCache = config.temp + config.templateCache.file,
		cssFilter = $.filter('**/*.css', {restore: true}),
		jsLibFilter = $.filter('**/' + config.optimised.lib, {restore: true}),
		jsAppFilter = $.filter('**/' + config.optimised.app, {restore: true}),
		indexHtmlFilter = $.filter(['**', '!**/index.html'], {restore: true}),
		assets = $.useref({searchPath: './'});

	return gulp
		.src(config.index)
		.pipe($.plumber())
		.pipe($.inject(gulp.src(templateCache, {read: false}), {
			starttag: '<!-- inject:templates:js -->'
		}))
		.pipe(assets)
		.pipe(cssFilter)
		.pipe($.csso())
		.pipe(cssFilter.restore)
		.pipe(jsLibFilter)
		.pipe($.uglify())
		.pipe(jsLibFilter.restore)
		.pipe(jsAppFilter)
		.pipe($.uglify())
		.pipe(jsAppFilter.restore)
		.pipe(indexHtmlFilter)
		.pipe($.rev())
		.pipe(indexHtmlFilter.restore)
		.pipe($.revReplace())
		.pipe(gulp.dest(config.public))
		.pipe($.rev.manifest())
		.pipe(gulp.dest(config.public));
});

gulp.task('clean', function(done) {
	var delconfig = [].concat(config.public, config.temp);
	log('Cleaning: ' + $.util.colors.green(delconfig));
	return del(delconfig);
});

gulp.task('clean-styles', function(done) {
	return clean(config.temp + '**/*.css');
});

gulp.task('clean-code', function(done) {
	var files = [].concat(
		config.temp + '**/*.js',
		config.public + '**/*.html',
		config.public + 'js/**/*.js',
		config.public + 'css/**/*.css'
	);
	return clean(files);
});

gulp.task('clean-fonts', function(done) {
	return clean(config.public + 'fonts/**/*.*');
});

gulp.task('clean-images', function(done) {
	return clean(config.public + 'images/**/*.*');
});

gulp.task('serve-build', ['optimise'], function() {
	serve(false /* isDev */);
});

gulp.task('serve-dev', ['inject'], function() {
	serve(true /* isDev */);
});

gulp.task('test', ['unit-test', 'e2e-test']);

gulp.task('webdriver_standalone', webdriver_standalone);

gulp.task('webdriver_update', webdriver_update);

gulp.task('e2e-test', ['webdriver_update'], function(done) {
	return gulp
		.src(config.protractor)
		.pipe(protractor({
			configFile: __dirname + '/protractor.conf.js',
			args: ['--baseUrl', 'http://localhost:52000']
		}))
		.on('error', function(e) {
			console.log(e);
		});
});

gulp.task('unit-test', function(done) {
	return new Server({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, done).start();
});

gulp.task('tdd', function() {
	return new Server({
		configFile: __dirname + '/karma.conf.js',
	}).start();
});

//////// FUNCTIONS ////////

function serve(isDev) {
	var nodeOptions = {
		script: config.nodeServer,
		delayTime: 0,
		env: {
			'PORT': port,
			'NODE_ENV': isDev ? 'dev' : 'build'
		},
		watch: [config.server]
	};

	return $.nodemon(nodeOptions)
		.on('restart', function(ev) {
			log('*** Nodemon restarted ***');
			log('Files changed on restart:\n' + ev);
			setTimeout(function() {
				browserSync.notify('reloading now...');
				browserSync.reload({stream: false});
			}, config.browserReloadDelay);
		})
		.on('start', function() {
			log('*** Nodemon started ***');
			startBrowserSync(isDev);
		})
		.on('crash', function() {
			log('*** Nodemon crashed ***');
		})
		.on('exit', function() {
			log('*** Nodemon exited cleanly');
		});
}

function startBrowserSync(isDev) {
	if(args.nosync || browserSync.active) {
		return;
	}

	log('Starting browser-sync on port ' + port);

	if(isDev) {
		gulp.watch([config.sassWatch], ['styles'])
		.on('change', function(event) { changeEvent(event); });
	} else {
		gulp.watch([config.sassWatch, config.js, config.html], ['optimise', browserSync.reload])
		.on('change', function(event) { changeEvent(event); });
	}

	var options = {
		proxy: 'localhost:' + port,
		port: 80,
		host: 'drupal.vmdev.sbs.com.au',
		ws: true,
		files: isDev ? [
			config.client + '**/*.*',
			'!' + config.sass,
			config.temp + '**/*.css'
		] : [],
		ghostMode: {
			clicks: true,
			location: false,
			forms: true,
			scroll: true
		},
		injectChanges: true,
		logFileChanges: true,
		logLevel: 'debug',
		logPrefix: 'gulp-patterns',
		notify: true,
		reloadDelay: 0
	};
	browserSync(options);
}

function changeEvent(event) {
	var srcPattern = new RegExp('/.*(?=/' + config.public + ')/');
	log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}

function clean(path) {
	log('Cleaning: ' + $.util.colors.green(path));
	return del(path);
}

function log(msg) {
	if(typeof(msg) === 'object') {
		for(var item in msg) {
			$.util.log($.util.colors.green(msg));
		}
	} else {
		$.util.log($.util.colors.green(msg));
	}
}
