
You will need:
Node.js
java
and live-reload extention for your browser

- open cmd and clone repo
- run 'npm install'
- run 'npm start' // This will start up using production files

to execute the test suite(unit & e2e)
- open cmd and run 'npm start'
- then open a new cmd and run 'gulp webdriver_standalone' to start the selenium server
- and then in a third cmd window run, 'npm test'

to start in development mode run 'gulp serve-dev'
and in a seperate cmd window, run 'gulp tdd' to have the unit tests watching for changes
