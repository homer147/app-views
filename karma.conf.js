module.exports = function(config) {
	config.set({
		frameworks: ['jasmine'],
		reporters: ['spec'],
		browsers: ['PhantomJS'],
		files: [
			'./bower_components/angular/angular.min.js',
			'./bower_components/angular-ui-router/release/angular-ui-router.js',
			'./bower_components/angular-touch/angular-touch.js',
			// my js
			'./src/client/tests-unit/**/*.js'
		]
	});
};