module.exports = function() {
	var client = './src/client/',
		clientApp = client + 'app/',
		server = './src/server/',
		temp = './.tmp/';

	config = {

		// File Paths
		client: client,
		css: temp + 'styles.css',
		fonts: [
			'./bower_components/font-awesome/fonts/**/*.*',
			client + 'fonts/**/*.*'
		],
		html: client + '**/*.html',
		htmltemplates: client + '**/*.html',
		images: client + 'images/**/*.*',
		index: client + 'index.html',
		js: [
			clientApp + '**/*.module.js',
			clientApp + '**/*.js',
			'!' + clientApp + '**/*.spec.js'
		],
		protractor: client + '/tests-unit/**/*.js',
		public: './public/',
		sass: client + '/styles/styles.scss',
		sassWatch: [
			client + 'styles/**/*.scss',
			clientApp + '**/*.scss'
		],
		server: server,
		temp: temp,

		// Bower and NPM locations
		bower: {
			json: require('./bower.json'),
			directory: './bower_components/',
			ignorePath: '../..',
			exclude: ['bootstrap/dist/js/', 'jquery', 'angular-mocks']
		},

		// Template cache
		templateCache: {
			file: 'templates.js',
			options: {
				module: 'loginApp',
				standAlone: false,
				root: '/'
			}
		},

		// Optimised
		optimised: {
			app: 'app.js',
			lib: 'lib.js'
		},

		// Browser Sync
		browserReloadDelay: 1000,

		// Node
		defaultPort: 51000,
		nodeServer: './src/server/login.js'
	};

	config.getWiredepDefaultOptions = function() {
		var options = {
			bowerJson: config.bower.json,
			directory: config.bower.directory,
			ignorePath: config.bower.ignorePath,
			exclude: config.bower.exclude
		};
		return options;
	};

	return config;
};
