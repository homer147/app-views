(function(){
	'use strict';

	var app = angular.module('loginApp');

	app.controller('forgotPasswordCtrl', ['$scope', '$rootScope', 'svrAuth', 'svrValidation',
		function($scope, $rootScope, svrAuth, svrValidation) {
			$scope.model = { email: null, fpPage: 1 };

			$scope.submitForgotPassword = function() {
				$rootScope.traditional = {};
				$rootScope.traditional.email = $scope.model.email;
				$rootScope.spinner = true;

				// This is quite slow... need to refactor
				svrValidation.checkEmailAvailability(function(res) {
					if(res.data.availability.response.available === false) {
						svrAuth.forgotPassword(function(res) {
							$scope.fp = res;
							$scope.model.fpPage++;
							$scope.emailSent = $scope.fp.data.forgotpassword.response.emailSent;
						}, function(res) {
							console.log('fail: ' + res);
						});
					}
				}, function(res) {
					console.log(res);
				});
			};
	}]);
}());