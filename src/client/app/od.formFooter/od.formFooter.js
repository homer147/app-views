(function(){
	'use strict';

	var app = angular.module('loginApp');

	app.directive('formFooter', function() {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: '/app/od.formFooter/od.formFooter.html'
		};
	});
}());