(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.controller('createAccountCtrl', ['$scope', '$rootScope', '$timeout', 'svrAuth',
		function($scope, $rootScope, $timeout, svrAuth) {
			$scope.model = {
				'nextDisabled': true,
				'formValues': {},
				'createLoginPage': 1
			};

			$scope.traditionalRegistration = function() {
				$rootScope.traditional.user = {};
				$rootScope.traditional.user.firstName = $scope.model.formValues.firstName;
				$rootScope.traditional.user.lastName = $scope.model.formValues.lastName;
				$rootScope.traditional.user.email = $scope.model.formValues.email;
				$rootScope.traditional.user.password = $scope.model.formValues.password;
				$rootScope.traditional.user.displayName = $scope.model.formValues.displayName;
				$rootScope.traditional.user.dob = $scope.model.formValues.dob;
				$rootScope.traditional.user.gender = $scope.model.formValues.gender;

				svrAuth.traditionalRegistration(function(res) {
					console.log(res);
				}, function(res) {
					console.log(res);
				});
			};

			$scope.next = function() {
				if($scope.model.createLoginPage < 3 && !$scope.model.nextDisabled) {
					$scope.model.createLoginPage++;
					$scope.model.nextDisabled = true;
				} else {
					$scope.badClick = true;
					$timeout(function() {
						$scope.badClick = false;
					}, 300);
				}
			};

			$scope.stopEvent = function($event) {
				$event.stopImmediatePropagation();
			};

			$scope.pbPage = function() {
				switch($scope.model.createLoginPage) {
					case 1:
						return 'pb-page-1';
					case 2:
						return 'pb-page-2';
					case 3:
						return 'pb-page-3';
					default:
						return 'pb-page-1';
				}
			};
	}]);
}());