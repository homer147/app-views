(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.controller('od.createAccountSocialCtrl', ['$scope', '$rootScope', '$stateParams', '$window',
		function($scope, $rootScope, $stateParams, $window) {
			$scope.token = $stateParams.access_token;
			$scope.tokenExp = $stateParams.expires_in;
			$scope.oldState = $window.localStorage.getItem('state');
			$scope.newState = $stateParams.state;
			$window.localStorage.removeItem('state');

			if ($scope.oldState === $scope.newState) {
				console.log('states are matching');
				if (!$rootScope.social.user) {
					console.log('no email provided');
				} else {
					console.log('email has been provided');
				}
			} else {
				console.log('we have an imposter!');
			}

			/*$scope.confirmDetails = function() {
				$state.go('signinVerify');
			};*/
	}]);
}());