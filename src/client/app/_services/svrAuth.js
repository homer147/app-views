(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.service('svrAuth', ['$rootScope', '$http', function($rootScope, $http) {

		var apiRoot = '/api/',
			fbGraphApi = 'https://graph.facebook.com/v2.7';

		var serializeObj = function(obj) {
		    var result = [];
		    for (var property in obj)
		        result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));

		    return result.join("&");
		};

		var getUserInfo = function() {
			var _self = this,
				rsSocial = $rootScope.social;

			FB.api('/me?fields=id,name,first_name,last_name,birthday,gender,email', function(response) {
				$rootScope.$apply(function() {
					$rootScope.social.user = _self.user = response;
				});
				if (rsSocial.checkStatusResponse && rsSocial.checkStatusResponse.status === 'connected') {
					$rootScope.fbButtonLabel = 'Or sign in as ' +  rsSocial.user.first_name;
				}
			});
		};

		var checkSocialStatus = function() {
			var _self = this;

			FB.getLoginStatus(function(response) {
				if(response.status === 'connected') {
					// logged into fb and have given permissions to our app
					$rootScope.social.checkStatusResponse = response;
					_self.getUserInfo(); 
				}
			});
		};

		var socialLogin = function(token, success, error) {
			var params = {
				client_id: 'gz8548gg77wymnkdnhbxyxxf8mccp6e2',
				flow: 'signIn',
				flow_version: '20160629171139095911',
				locale: 'en-US',
				redirect_uri: 'http://www.sbs.com.au/',
				response_type: 'token',
				token: token
			};

			$http({
				method: 'POST',
				url: 'https://sbs.janraincapture.com/oauth/auth_native',
				data: serializeObj(params),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			})
			.then(function(response) {
				console.log(response);
			}, function(response) {
				console.log(response);
			});
		};

		var traditionalLogin = function(success, error) {
			var params = {
				user: $rootScope.traditional.email,
				pass: $rootScope.traditional.password
			};

			$http({
				method: 'POST',
				url: apiRoot + 'v3/janrain/auth_native_traditional',
				data: serializeObj(params),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			})
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var checkEmailVerificationComplete = function(token, success, error) {
			$http.get(apiRoot + 'v3/member/verificationstatus?auth_token=' + token)
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var forgotPassword = function(success, error) {
			var email = $rootScope.traditional.email;

			$http.get(apiRoot + 'v3/member/forgotpassword?detail=' + email)
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var checkProfileStatus = function(token, success, error) {
			$http.get(apiRoot + 'v3/member/profilestatus?context=ios&auth_token=' + token)
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var completeLogin = function(token, success, error) {
			var params = {
				auth_token: token,
				context: 'ios'
			};

			$http({
				method: 'POST',
				url: apiRoot + 'v3/member/completelogin',
				data: serializeObj(params),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			})
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var profileUpdate = function(token, success, error) {
			var missingFields = $rootScope.traditional.missingFields,
				params = {
					auth_token: token
				};

			angular.forEach(missingFields, function(field) {
				params[field] = $rootScope.traditional[field];
			});

			$http({
				method: 'POST',
				url: apiRoot + 'v3/member/profileupdate',
				data: serializeObj(params),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			})
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var sendVerificationEmail = function(token, success, error) {
			$http.get(apiRoot + 'v3/member/resendverification?auth_token=' + token)
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		var traditionalRegistration = function(success, error) {
			var params = {
				context: 'ios',
				givenName: $rootScope.traditional.user.firstName,
				familyName: $rootScope.traditional.user.lastName,
				gender: $rootScope.traditional.user.gender,
				birthday: $rootScope.traditional.user.dob,
				email: $rootScope.traditional.user.email,
				username: $rootScope.traditional.user.displayName,
				password: $rootScope.traditional.user.password
			};

			$http({
				method: 'POST',
				url: apiRoot + 'v3/member/register',
				data: serializeObj(params),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			})
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		return {
			getUserInfo: getUserInfo,
			checkSocialStatus: checkSocialStatus,
			forgotPassword: forgotPassword,
			traditionalLogin: traditionalLogin,
			checkEmailVerificationComplete: checkEmailVerificationComplete,
			checkProfileStatus: checkProfileStatus,
			completeLogin: completeLogin,
			profileUpdate: profileUpdate,
			sendVerificationEmail: sendVerificationEmail,
			socialLogin: socialLogin,
			traditionalRegistration: traditionalRegistration,
			serializeObj: serializeObj
		};
	}]);
}());