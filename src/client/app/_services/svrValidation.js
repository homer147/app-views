(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.service('svrValidation', ['$rootScope', '$http', function($rootScope, $http) {
		var apiRoot = '//www.sbs.com.au/api/';

		var checkEmailAvailability = function(success, error) {
			var email = $rootScope.traditional.email;

			$http.get(apiRoot + 'v3/member/availability?type=email&data=' + email)
			.then(function(response) {
				success(response);
			}, function(response) {
				error(response);
			});
		};

		return {
			checkEmailAvailability: checkEmailAvailability
		};
	}]);
}());