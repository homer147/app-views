(function(){
	'use strict';

	var app = angular.module('loginApp');

	app.directive('formValidation', ['$rootScope', '$timeout', 'svrValidation', function($rootScope, $timeout, svrValidation) {
		return function(scope, elem, attrs) {

			var formName = attrs.name,
				form = scope[formName],
				emailField = angular.element(elem[0].querySelector('.email')),
				errorEmpty = angular.element(elem[0].querySelectorAll('.error-empty')),
				caNext = angular.element(elem[0].querySelector('.ca-next')),
				caFirstName = angular.element(elem[0].querySelector('.ca-first-name')),
				caLastName = angular.element(elem[0].querySelector('.ca-last-name')),
				caEmail = angular.element(elem[0].querySelector('.ca-email')),
				caPassword = angular.element(elem[0].querySelector('.ca-password')),
				caDisplayName = angular.element(elem[0].querySelector('.ca-display-name')),
				lgPassword = angular.element(elem[0].querySelector('.lg-password')),
				caTimer;

			var functions = {
				clearValidation: function(error) {
					error.text(null);
					if(form.$name === 'forgotPassword')
						scope.$apply(function() {
							scope.model.emailRecognised = true;
						});
				},
				caNextClick: function() {
					if(caNext[0].className.indexOf('ca1') > -1) {
						var errorEmptyFN = caFirstName.next('error-empty-msg'),
							errorEmptyLN = caLastName.next('error-empty-msg'),
							NameFN = caFirstName[0].attributes.name.value,
							NameLN = caLastName[0].attributes.name.value;

						functions.isErrorEmpty(errorEmptyFN, NameFN);
						functions.isErrorEmpty(errorEmptyLN, NameLN);
					}
					if(caNext[0].className.indexOf('ca2') > -1) {
						var errorEmptyPw = caPassword.next('error-empty-msg'),
							errorEmptyDN = caDisplayName.next('error-empty-msg'),
							namePw = caPassword[0].attributes.name.value,
							nameDN = caDisplayName[0].attributes.name.value;

						functions.email.validate();
						functions.isErrorEmpty(errorEmptyPw, namePw);
						functions.isErrorEmpty(errorEmptyDN, nameDN);
					}
				},
				caNextbutton: function() {
					if(caNext[0].className.indexOf('ca1') > -1) {
						var fnField = form.firstName,
							lnField = form.lastName;

						if(fnField.$valid && lnField.$valid) {
							scope.model.nextDisabled = false;
							scope.$apply();
						} else {
							scope.model.nextDisabled = true;
							scope.$apply();
						}
					}
					if(caNext[0].className.indexOf('ca2') > -1) {
						var emField = form.email,
							pwField = form.password,
							dnField = form.displayName;

						if(emField.$valid && pwField.$valid && dnField.$valid) {
							scope.model.nextDisabled = false;
							scope.$apply();
						} else {
							scope.model.nextDisabled = true;
							scope.$apply();
						}
					}
				},
				email: {
					validate: function() {
						var field = form.email,
							emailError = angular.element(elem[0].getElementsByClassName('email-error'));
						if (field.$error.required && field.$invalid) {
							emailError.text('This field is required');
						}
						if (field.$error.email && field.$invalid) {
							emailError.text('Please enter a valid email');
						}
						if (field.$valid) {
							var positiveCheck = form.$name === 'traditionalLoginForm' || form.$name === 'forgotPassword';
							if (form.$name === 'traditionalLoginForm')
								scope.model.emailRecognised = false;
							svrValidation.checkEmailAvailability(function(res) {
								if(positiveCheck && res.data.availability.response.available === true) {
									emailError.text('Email address not recognised');
									$rootScope.traditional.badEmail = true;
									$rootScope.spinner = false;
									scope.model.emailRecognised = false;
								} else {
									scope.model.emailRecognised = true;
								}
							}, function(res) {
								console.log('call doesnt work');
							});
						}
					},
				},
				isErrorEmpty: function(errorEmptyError, input) {
					var field = form[input];
					if (field.$invalid && (field.$viewValue === undefined || field.$viewValue === '' || field.$viewValue === null)) {
						errorEmptyError.text('This field is required');
					}
				},
				keydown: function(e) {
					if(e && e.srcElement.className.indexOf(' ng-valid ') > -1) {
						e.srcElement.nextElementSibling.innerText = '';
					}
					$timeout.cancel(caTimer);
					caTimer = $timeout(function() {
						functions.caNextbutton();
					}, 500);
				}
			};

			// General form validation

			if(errorEmpty) {
				angular.forEach(errorEmpty, function(value) {
					value = angular.element(value);

					value.bind('focus', function() {
						var errorEmptyError = value.next('error-empty-msg');
						functions.clearValidation(errorEmptyError);
					});

					value.bind('blur', function() {
						var errorEmptyError = value.next('error-empty-msg'),
							inputName = value[0].attributes.name.value;
						functions.isErrorEmpty(errorEmptyError, inputName);
					});
				});
			}

			if(emailField) {
				emailField.bind('focus', function() {
					var emailEmptyError = emailField.next('email-error');
					functions.clearValidation(emailEmptyError);
				});

				emailField.bind('blur', function() {
					var error = emailField.next('.email-error'),
						inputName = emailField[0].attributes.name.value;
					$rootScope.traditional = {};
					$rootScope.traditional.email = scope.model.email;
					functions.email.validate();
				});
			}

			if(lgPassword) {
				lgPassword.bind('keydown', function() {
					scope.$apply(function() {
						scope.model.passwordError = false;
					});
				});
			}

			// Create Account Carousel Validation

			if(caNext[0]) {
				caFirstName.bind('keydown', functions.keydown);
				caLastName.bind('keydown', functions.keydown);
				caEmail.bind('keydown', functions.keydown);
				caPassword.bind('keydown', functions.keydown);
				caDisplayName.bind('keydown', functions.keydown);
				caNext.bind('mousedown', functions.caNextClick);

				scope.$watch('model.noScroll', function() {
					if(scope.model.noScroll === true) {
						functions.caNextClick();
					}
				});

				scope.$watch('model.createLoginPage', function() {
					functions.keydown();
				});
			}
		};
	}]);
}());
