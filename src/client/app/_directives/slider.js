(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.directive('slider', ['$swipe', '$window', function($swipe, $window) {
		return function(scope, elem) {
			angular.element(document).ready(function() {
				var el = elem[0],
					oldX = 0,
					newX = 0,
					change,
					distance,
					divPosition,
					fib = [0, 1],
					left,
					scrollable = angular.element(elem[0].querySelector('.scroll-content')),
					styles;

				var drag = function() {
					distance = oldX - newX;

					divPosition = left + distance;

					if(oldX > newX) {
						if(scope.model.createLoginPage === 1 || scope.obPage === 1) {
							distance = distance / 12;
							divPosition = left + distance;
							if(distance > 45) {
								divPosition = left + 45;
							}
						} else if(distance > 50) {
							change = 'previous';
						}
						scrollable.css({'left': divPosition + 'px', 'transition-property': 'none'});
					}

					if(newX > oldX) {
						if(scope.model.createLoginPage === 3 || scope.model.nextDisabled) {
							if (scope.model.nextDisabled) {
								scope.model.noScroll = true;
								scope.$apply();
							}
							distance = distance / 12;
							divPosition = left + distance;
							if(distance < -45) {
								divPosition = left - 45;
							}
						} else if(distance < -50) {
							change = 'next';
						}
						scrollable.css({'left': divPosition + 'px', 'transition-property': 'none'});
					}
				};

				var dragEnd = function() {
					if(scrollable) {
						scope.model.noScroll = false;
						scope.$apply();
						scrollable.css({'left': '', 'transition-property': ''});

						switch(change) {
							case 'next':
								if(scope.model.createLoginPage && scope.model.createLoginPage < 3) {
									scope.model.createLoginPage++;
									scope.$apply();
								}
								if(scope.obPage && scope.obPage < 3) {
									scope.obPage++;
									scope.$apply();
								}
								break;

							case 'previous':
								if(scope.model.createLoginPage && scope.model.createLoginPage > 1) {
									scope.model.createLoginPage--;
									scope.$apply();
								}
								if(scope.obPage && scope.obPage > 1 && scope.obPage < 3) {
									scope.obPage--;
									scope.$apply();
								}
								break;

							default:
								break;
						}
					}
					change = null;
				};

				el.addEventListener('touchstart', function(e) {
					styles = $window.getComputedStyle(scrollable[0], null);
					left = parseInt(styles.left);
					oldX = e.targetTouches[0].pageX;
					newX = e.targetTouches[0].pageX;
				}, false);

				el.addEventListener('touchmove', function(e) {
					oldX = e.targetTouches[0].pageX;
					requestAnimationFrame(drag);
				}, false);

				el.addEventListener('touchend', function(e) {
					requestAnimationFrame(dragEnd);
				}, false);
	        });
		};
	}]);

	app.directive('verticalSlider', ['$timeout', function($timeout) {
		return function(scope, elem, attrs) {
			scope.showDetails = false;

			scope.$watch('showDetails', function() {
				switch(scope.showDetails) {
					case true:
						attrs.$set('style', 'max-height: none');
						var height = elem[0].offsetHeight;
						attrs.$set('style', 'max-height: 0');
						$timeout(function() {
							attrs.$set('style', 'max-height: ' + height + 'px');
						}, 20);
						break;
					default:
						attrs.$set('style', 'max-height: 0');
						break;
				}
			});
		};
	}]);
}());