(function(){
	'use strict';

	var app = angular.module('loginApp');

	app.directive('spinner', ['$rootScope', '$http', function($rootScope, $http) {
		return {
			restrict: 'E',
			replace: true,
			template: '<div class="loading-spinner" ng-show="spinner" spinner></div>',
			link: function(scope, elem, attrs) {
				$rootScope.spinner = false;
				scope.isLoading = function() {
					if ($http.pendingRequests.length > 0 && $http.pendingRequests[0].url.indexOf('member/availability') > -1) {
						return false;
					} else {						
						return $http.pendingRequests.length > 0;
					}
				};

				scope.$watch(scope.isLoading, function(res) {
					if (res) {
						$rootScope.spinner = true;
					} else {
						$rootScope.spinner = false;
					}
				});
			}
		};
	}]);
}());