(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.directive('fade', function() {
		return function(scope, elem) {
			var element = angular.element(elem);

			element.addClass('od-fade');
			element.addClass('fade-out');

			angular.element(document).ready(function() {
				element.removeClass('fade-out');
			});
		};
	});
}());