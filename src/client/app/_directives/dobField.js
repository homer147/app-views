(function(){
	'use strict';

	var app = angular.module('loginApp');

	app.directive('dobField', ['dateFilter', function(dateFilter) {
		return {
			require: 'ngModel',
			link: function(scope, elem, attrs, ctrl) {
				var dateFormat = 'dd/MM/yyyy',
					ogDateFormat = 'yyyy-MM-dd',
					pattern = /(\d{2})(\d{2})(\d{4})/,
					ogDate,
					formattedDate;

				elem[0].type = 'text';

				elem.bind('focus', function() {
					ogDate = elem[0].value;
					ogDate = ogDate.replace(/\//ig, '');
					elem[0].type = 'date';
					formattedDate = ogDate.replace(pattern, '$3-$2-$1');
					ctrl.$setViewValue(formattedDate);
					elem[0].value = formattedDate;
				});

				elem.bind('blur', function() {
					elem[0].type = 'text';

					formattedDate = dateFilter(elem[0].value, dateFormat);
					ctrl.$setViewValue(formattedDate);
					elem[0].value = formattedDate;
				});
			}
		};
	}]);
}());