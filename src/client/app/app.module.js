(function() {
	'use strict';

	var app = angular.module('loginApp', ['ui.router', 'ngTouch', 'ngSanitize']);

	app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
		function($stateProvider, $urlRouterProvider, $locationProvider) {
			$urlRouterProvider.otherwise('/');
			$stateProvider
				.state('home', {
					url: '/',
					templateUrl: 'app/od.onboarding/od.onboarding.html',
					controller: 'od.onboardingCtrl'
				})
				.state('createAccount', {
					url: '/createAccount.html',
					templateUrl: 'app/od.createAccount/od.createAccount.html',
					controller: 'createAccountCtrl'
				})
				.state('createAccountSocial', {
					url: '/createAccountSocial.html?:state&:access_token&:expires_in',
					templateUrl: 'app/od.createAccountSocial/od.createAccountSocial.html',
					controller: 'od.createAccountSocialCtrl',
					resolve: {
                        'urlFix': ['$location', function($location){
                            $location.url($location.url().replace("#","?"));
                        }]
                    }
				})
				.state('faq', {
					url: '/faqs.html',
					templateUrl: 'app/od.faq/od.faq.html',
					controller: 'faqCtrl'
				})
				.state('forgotPassword', {
					url: '/forgotPassword.html',
					templateUrl: 'app/od.forgotPassword/od.forgotPassword.html',
					controller: 'forgotPasswordCtrl'
				})
				/*.state('signinLater', {
					url: '/signinlater.html',
					templateUrl: '/app/od.signinlater/od.signinlater.html'
				})*/
				.state('signinVerifyEmailMsg', {
					url: '/signinVerifyEmailMsg.html',
					templateUrl: 'app/od.signinVerifyEmailMsg/od.signinVerifyEmailMsg.html',
					controller: 'od.verifyEmailCtrl'
				})
				.state('signinVerify', {
					url: '/signinVerify.html',
					templateUrl: 'app/od.signinVerify/od.signinVerify.html',
					controller: 'od.signinVerify'
				});

			$locationProvider.html5Mode(true);
		}]);

	app.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
	}]);

	app.run(['$rootScope', '$window', 'svrAuth', function($rootScope, $window, svrAuth) {
		var rsSocial = $rootScope.social = {};

		$rootScope.fbButtonLabel = 'Or sign in with Facebook';

		$window.fbAsyncInit = function() {
			// Execute when the sdk is loaded

			FB.init({
				// The id of the web app
				appId: '399908920106464',
				xfbml: true,
				version    : 'v2.7',

				// Set if you want to check the authentication status at the start up of the app
				status: true,

				//Enable cookies to allow the server to access the session
				cookie: true
			});

			svrAuth.checkSocialStatus();
		};

		(function(d, s, id){
		    var js,
		    	fjs = d.getElementsByTagName(s)[0];

			if (d.getElementById(id)) {return;}
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
	}]);
}());
