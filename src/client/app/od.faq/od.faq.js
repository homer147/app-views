(function(){
	'use strict';
	
	var app = angular.module('loginApp');

	app.controller('faqCtrl',  ['$scope', '$sce', function($scope, $sce) {

		var iframe = $sce.trustAsHtml('<p>We understand you might be interested in how we use the data associated with your SBS On Demand account. Our <a class="faq-link" href="http://www.sbs.com.au/aboutus/corporate/view/id/114/h/Terms-and-Conditions" target="_blank">Terms of Use</a> and <a class="faq-link" href="http://www.sbs.com.au/aboutus/corporate/view/id/113/h/SBS-Privacy-Statement" target="_blank">Privacy Policy</a> provides some clear, concise info explaining what we do with your data. At the heart of it, we use information on how people use our websites and applications to learn how we can improve them and the content they hold. By understanding which programmes you like, we can recommend others that you might also like but haven&#39;t yet seen. We can also use this information to create more shows we think you&#39;ll love.</p><p>We may also, from time to time, obtain additional information about you from other reputable data sources to understand more about who is using our products and services. When we do this, we may learn about your lifestyle and interests and can use this information, alongside cookies and similar technology such as Online Behavioural Advertising, to provide more relevant advertising to you. Our Cookies Policy [link] has more detail on this and can also help if you want to manage your cookies.</p><p>Still have concerns about Privacy? Lee Lin Chin explains.</p><div class="videoEmbed"><iframe src="https://www.youtube.com/embed/ohfJXkpF2JU?controls=0&amp;showinfo=0&amp;modestbranding=0" frameborder="0" allowfullscreen=""></iframe></iframe></div>');

		$scope.faqList = {

			login: {
				h3: 'LOGIN/CREATE AN ACCOUNT',
				item1: {
					title: 'Why do I need to login or create an account to use the updated SBS On Demand iOS app?',
					content: '<p>Signing in is quick and easy and allows you to:</p><p>Watch On Demand shows and movies for free</p><p>Experience exclusive previews of your favourite new programs</p><p>SBS is committed to being open and transparent about how we collect and use your data. Read our Viewer&#39;s Promise.</p><p>You will need to sign in to access SBS On Demand. We&#39;ve done this to ensure you get the most out of your SBS On Demand experience. It&#39;s quick, free and easy and means you can add programmes for later, and pick up watching programmes where you left off across your devices (the android app and website for now).</p><p>Log in to add videos that tickle your fancy to your FAVOURITES, receive NOTIFICATIONS on the shows you love and when they&#39;re about to expire, sync your favourites across other devices, and be able to CONTINUE WATCHING programs at a more convenient time.</p>'
				},
				item2: {
					title: 'What do you do with the information you collect?',
					content: iframe
				}
			},
			myAccount: {
				h3: 'MANAGING YOUR SBS ON DEMAND ACCOUNT',
				item1: {
					title: 'I&#39;ve forgotten my password',
					content: '<p>It happens. When you see the login form, there&#39;s a link named <em>Forgot your password?</em>. Just click on that link and enter your username or email address and we&#39;ll email you a password reset link.</p>'
				},
				item2: {
					title: 'How do I change my password?',
					content: '<p>If you know your password, but would like to change it, follow the steps below.</p><ol><li>Visit <a class="faq-link" href="http://www.sbs.com.au/mysbs/" target="_blank">www.sbs.com.au/mysbs/</a>.</li><li>Login</li><li>Once logged in, click <em>Change password</em></li></ol>'
				},
				item3: {
					title: 'What happened to my playlist ?',
					content: '<p>We&#39;ve made this change to fit how people are using SBS On Demand. Instead of adding to your playlist, you are now able to &#39;favourite&#39; any programmes or movies.</p>'
				}
			},
			favouriting: {
				h3: 'FAVOURITING',
				item1: {
					title: 'What are Favourites?',
					content: '<p>&#39;Favourites&#39; are a really easy way to save Programs or Movies to watch later. Just log in or sign up to start favouriting videos.</p><p>Tap the heart icon at the top right of the program or movie.</p>'
				}
			},
			notifications: {
				h3: 'NOTIFICATIONS',
				item1: {
					title: 'How do I manage my Notification settings?',
					content: '<p>Once you&#39;ve logged in, tap the PROFILE icon in the top right hand corner, select Notifications, and manage away.</p>'
				},
				item2: {
					title: 'What notifications are available?',
					content: '<p>Notify me if one of my favourites has a new episode</p><p>Notify me if my one of my favourites is about to expire</p>'
				},
				item3: {
					title: 'How do I stop receiving notifications?',
					content: '<p>Go to your device > Settings > Notifications >  SBS On Demand > swipe to disable</p>'
				}
			},
			technical: {
				h3: 'TECHNICAL',
				item1: {
					title: 'What are the minimum technical requirements for using this version of the SBS On Demand app?',
					content: '<p>The 2016 SBS On Demand app has been optimised to work on:</p><p>Tablets: iPad 2, iPad Mini, iPad Air 2 (running iOS 8+)<br />iPhones: 4, 4S, 5s, 6, 6 Plus (running iOS 8+)</p><p>The app is a universal iOS app, which means the same app is designed to work on all of the above devices.</p>'
				},
				item2: {
					title: 'Can I watch videos on my SBS On Demand app outside Australia?',
					content: '<p>Due to rights restrictions, we are only able to offer SBS On Demand videos to users accessing the service from Australia. Even if you are an Australian citizen, SBS On Demand content won&#39;t be available to you while you are abroad. As such, the SBS On Demand app is only available for purchase via the Australian iTunes store.</p>'
				},
				item3: {
					title: 'How much data is consumed playing an SBS On Demand video?',
					content: '<p>We&#39;ve come up with an approximate guide as to how much data is consumed watching a SBS On Demand video, depending on length, on a mobile network.</p><p>This assumes watching over a strong 3G connection, when data consumption is highest, and assumes that no data has been consumed other than via watching the video:</p><p>60 minute video - up to 675MB of data<br />30 minute video - up to 340MB of data<br />10 minute video - up to 115MB of data<br />5 minute video -  up to 50MB of dataWhat are the available bitrates?</p>'
				},
				item4: {
					title: 'What are the available bitrates?',
					content: '<p>Low: about 128kb/second<br />Medium: about 512kb/second<br />High: about 1Mb/second<br />Very High: about 1.5Mb/second<br />Auto: will switch between bitrates as network connection allows</p>'
				},
				item5: {
					title: 'How much will I be charged by my mobile phone provider for watching video on a 3G/4G network?',
					content: '<p>If you choose to watch videos on the SBS On Demand app over your mobile provider&#39;s network, you should take your data quota into consideration. This is the amount of data that your mobile phone company allows you to download each month at no extra cost.</p><p>Should you exceed this quota, you may need to pay for extra costs.</p><p>Prior to using the SBS On Demand app on a mobile network, SBS recommends that you confirm with your mobile phone provider what your data quota is, as well as what the costs are for exceeding this quota. It is important that you monitor your usage to ensure that you don&#39;t exceed your limit.</p>'
				},
				item6: {
					title: 'How do I use AirPlay to watch SBS On Demand on my TV?',
					content: '<p>If you own an Apple TV, you can use AirPlay to stream SBS On Demand to your TV. When you play a video on the SBS On Demand  iOS app (and your Apple TV is connected), you&#39;ll see a small TV icon on the video player controls.  Tap this and select the corresponding Apple TV from the drop-down menu. This will then stream the video to your TV.</p>'
				},
				item7: {
					title: 'How do I turn on closed captions?',
					content: '<p>To enable closed captions, please wait for the video&#39;s opening advertisement to finish, then tap on the video player to access the controls and tap the &#39;CC&#39; icon.</p>'
				}
			},
			questions: {
				h3: 'QUESTIONS ABOUT SBS ON DEMAND CATALOGUE',
				item1: {
					title: 'Why can&#39;t I find certain programs?',
					content: '<p>If you can&#39;t find a specific title in our Programs section, try using the SEARCH feature. If still no luck, this could be due to a number of reasons.<br />Either the episode or program is no longer available because it has expired, or<br />SBS may not have the rights to offer this program on our On Demand service. You can always check our handy online TV Guide to see which programs will be making their way to SBS On Demand after they air (anything with a &#39;play&#39; symbol will be available to catch up).</p>'
				},
				item2: {
					title: 'Why aren&#39;t all SBS programs available?',
					content: '<p>We can&#39;t always acquire SBS On Demand rights for every programme we broadcast, though we try to! This is sometimes due to the cost involved in acquiring licences from third parties or maybe for other legal or contractual reasons. And sometimes we&#39;ll only be able to acquire the rights to stream a programme, but not to download it.</p><p>You can always check our handy online TV Guide to see which programs will be making their way to SBS On Demand after they air (anything with a &#39;play&#39; symbol will be available to catch up).</p>'
				},
				item3: {
					title: 'How long are programs available for?',
					content: '<p>Most videos coming through to SBS On Demand as catch up from TV are available for 14 or 30 days. Sometimes we can only acquire rights for 7 days catch up. This does vary so we&#39;d suggest you check the video details or series page which will indicate the specific expiry date. We are able to offer a few programs indefinitely, such as  RocKwiz, Immigration Nation, Go Back To Where You Came From and Letters and Numbers.</p><p>We also have an expanded movies, drama and factual catalogue, where videos will generally be available from 6 months to one year, with new titles added each month.</p>'
				},
				item4: {
					title: 'Why do videos expire?',
					content: '<p>Videos expire because we only have rights from the distributors to offer them for a certain period.</p>'
				}
			},
			general: {
				h3: 'GENERAL',
				item1: {
					title: 'Can I access SBS On Demand on other platforms?',
					content: '<div class="faqsContent" style="display: block;"><p>You sure can! At last count we&#39;re available on 24 platforms.</p><p><strong>Computers</strong><br>Jump on a computer or laptop and head to <a class="faq-link" href="http://www.sbs.com.au/ondemand">www.sbs.com.au/ondemand</a>. We recommend using Google Chrome as your browser.</p><p><u>Windows</u><br>We have an app for Windows (7 and 8) that you can find in the Windows Store.</p><p><strong>Phone &amp; Tablet Apps</strong></p><p><u>Apple</u><br>iPad (iOS 8.0 and above)<br>iPhone (iOS 8.0 and above)<br>iPod touch (iOS 8.0 and above)</p><p><u>Android Phones</u><br><em>Requires Android 4.0 Ice Cream Sandwich or above</em><br>Samsung Galaxy S2, S3, S4<br>Samsung Galaxy Note 4<br>Samsung Galaxy 6 Note<br>Samsung Galaxy Note Edge<br>Samsung Galaxy S6 Edge<br>LG Nexus 4<br>LG Nexus 5<br>HTC One mini<br>Sony Xperia Z<br>Sony Xperia Z1<br>Sony Xperia Z Ultra<br>Samsung S4 mini</p><p><u>Android Tablets</u><br><em>Requires Android 4.0 Ice Cream Sandwich or above</em><br>Nexus 7 (2013)<br>Nexus 10<br>Sony Xperia Tablet Z<br>Asus Google Nexus 7<br>Asus Ee Pad Transformer</p><p><u>Amazon Kindle Fire</u><br>Kindle Fire (2nd Gen)<br>Kindle Fire HD 7 (2nd Gen)<br>Kindle Fire HD 8.9 (2nd Gen)<br>Kindle Fire HD 7 (3rd Gen)<br>Kindle Fire HDX 7 (3rd Gen)<br>Kindle Fire HDX 8.9 (3rd Gen)</p><p><u>Windows Phone</u><br>Windows Phone 7<br>Windows Phone 8</p><p><strong>Internet Enabled TVs</strong><br>Sony TVs &amp; Blu-ray players<br>Samsung TVs &amp; Blu-ray players<br>LG TVs<br>Panasonic TVs<br>Hisense TVs<br>TCL TVs<br>HbbTV/Freeview certified TVs</p><p><strong>Gaming console</strong><br>Microsoft Xbox 360<br>Xbox One<br>Sony PlayStation 3<br>Sony PlayStation 4</p><p><strong>Set-top boxes</strong><br>Apple TV 4th gen<br>Fetch TV set top boxes with Internode, iiNet, or Optus<br>TCL internet-enabled TVs<br>Telstra TV<br>Telstra T-box</p></div>'
				}
			}
		};

	}]);
}());