(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.controller('od.onboardingCtrl', ['$rootScope', '$scope', '$location', '$window', 'svrAuth', 'svrValidation',
		function($rootScope, $scope, $location, $window, svrAuth, svrValidation) {

			
			//$scope.fbUrl = 'https://www.facebook.com/dialog/oauth?' + data;
			$scope.obPage = 1;
			$scope.model = {
				email: null,
				password: null,
				emailRecognised: false,
				passwordError: false
			};

			$scope.next = function(gotoPage) {
				if($scope.obPage < 3) {
					$scope.obPage++;
				}
			};

			$scope.changeScreen = function(screenNo) {
				$scope.obPage = screenNo;
			};

			$scope.previous = function() {
				if($scope.obPage > 1) {
					$scope.obPage--;
				}
			};

			$scope.pbPage = function() {
				switch($scope.obPage) {
					case 1:
						return 'pb-page-1';
					case 2:
						return 'pb-page-2';
					case 3:
						return 'pb-page-3';
					default:
						return 'pb-page-1';
				}
			};

			$scope.stopEvent = function($event) {
				$event.stopImmediatePropagation();
			};

			$scope.socialLogin = function() {
				var state = randomString(200);
				$window.localStorage.setItem('state', state);
				var params = {
						client_id: '399908920106464',
						redirect_uri: 'http://drupal.vmdev.sbs.com.au/createAccountSocial.html',
						response_type: 'token',
						scope: 'email,user_birthday,publish_actions',
						state: state
					},
					data = svrAuth.serializeObj(params),
					url = 'https://www.facebook.com/dialog/oauth?' + data;

				$window.location.href = url;
			};

			$scope.traditionalLogin = function() {
				$rootScope.traditional = {};
				$rootScope.traditional.email = $scope.model.email;
				$rootScope.traditional.password = $scope.model.password;

				// authenticate with janrain
				svrAuth.traditionalLogin(function(res) {
					delete $rootScope.traditional.password;
					if(res.data.stat === 'error') {
						$scope.model.passwordError = true;
					} else {
						var token = res.data.access_token;
						if(token) {
							checkEmailVerification(token);
						}
					}
				}, function(res) {
					delete $rootScope.traditional.password;
					console.log('no connection');
					// TODO - handle no connection
				});
			};

			var checkEmailVerification = function(token) {
				// has the provided email address been verified?
				svrAuth.checkEmailVerificationComplete(token, function(res) {
					if (res.data.verificationstatus.response.emailVerified === true) {
						profileStatus(token);
					} else {
						// email not verified
						$rootScope.traditional.token = token;
						$location.url('/signinVerifyEmailMsg.html');
					}
				}, function(res) {
					console.log(res);
					console.log('lost connection');
				});
			};

			var profileStatus = function(token) {
				// do we have all required details for user account?
				svrAuth.checkProfileStatus(token, function(res) {
					if (res.data.profilestatus.response.complete === false) {
						var missing = res.data.profilestatus.response.missing;
						$rootScope.traditional.missingFields = missing;
						$rootScope.traditional.token = token;
						$location.url('/signinVerify.html');
					} else {
						svrAuth.completeLogin(token, function(res) {
							// done! tell the app to close this webview
							console.log(res);
							console.log('nice work. now back to the app!');
							// TODO - redirect to app
						}, function(res) {
							console.log(res);
							console.log('lost connection');
						});
					}
				}, function(res) {
					console.log(res);
					console.log('lost connection');
				});
			};

			var randomString = function (len, bits) {
			    bits = bits || 36;
			    var outStr = "", newStr;
			    while (outStr.length < len)
			    {
			        newStr = Math.random().toString(bits).slice(2);
			        outStr += newStr.slice(0, Math.min(newStr.length, (len - outStr.length)));
			    }
			    return outStr.toUpperCase();
			};
	}]);
}());