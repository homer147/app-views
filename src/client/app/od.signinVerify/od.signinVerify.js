(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.controller('od.signinVerify', ['$scope', '$rootScope', '$location', 'svrAuth', function($scope, $rootScope, $location, svrAuth) {
		if($rootScope.traditional) {
			$scope.missing = $rootScope.traditional.missingFields;

			$scope.missingFieldMatch = function(string) {
				var match = false;
				angular.forEach($scope.missing, function(value) {
					if (value === string) {
						match = true;
					}
				});
				return match;
			};
		}

		$scope.completeLogin = function() {
			var token = $rootScope.traditional.token;
			delete $rootScope.traditional.token;
			// update profile
			updateProfileDetails(token);
		};

		var profileStatus = function(token) {
			// do we have all required details for user account?
			svrAuth.checkProfileStatus(token, function(res) {
				if (res.data.profilestatus.response.complete === false) {
					var missing = res.data.profilestatus.response.missing;
					$rootScope.traditional.missingFields = missing;
					$rootScope.traditional.token = token;
					console.log('still missing fields');
				} else {
					// create session.
					svrAuth.completeLogin(token, function(res) {
						console.log(res);
						console.log('logged in... finally. I still need to redirect to a url with the session id as a parameter');
						// this is working. i console logged a session id
						// i need to redirect to a url with the session id as a parameter
					}, function(res) {
						console.log(res);
					});
				}
			}, function(res) {
				console.log(res);
			});
		};

		var updateProfileDetails = function(token) {
			var fields = $rootScope.traditional.missingFields;
			angular.forEach(fields, function(field) {
				$rootScope.traditional[field] = $scope.signinVerify[field].$viewValue;
			});
			svrAuth.profileUpdate(token, function(res) {
				// check profile for missing fields again
				profileStatus(token);
			}, function(res) {
				console.log(res);
			});
		};
	}]);
}());