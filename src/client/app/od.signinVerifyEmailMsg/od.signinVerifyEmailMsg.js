(function() {
	'use strict';

	var app = angular.module('loginApp');

	app.controller('od.verifyEmailCtrl', ['$scope', '$rootScope', '$location', 'svrAuth', 
		function($scope, $rootScope, $location, svrAuth) {
			$scope.vePage = 1;
			if ($rootScope.traditional) {
				var token = $rootScope.traditional.token;

				$scope.sendVerificationEmail = function() {
					svrAuth.sendVerificationEmail(token, function(res) {
						$scope.vePage++;
					}, function(res) {
						console.log(res);
					});
				};

				$scope.signIntoApp = function() {
					// check again that email address is verified
					checkEmailVerification();
				};

				var checkEmailVerification = function() {
					// has the provided email address been verified?
					svrAuth.checkEmailVerificationComplete(token, function(res) {
						if (res.data.verificationstatus.response.emailVerified === true) {
							profileStatus();
						} else {
							// email still not verified
							$rootScope.traditional.token = token;
							$scope.vePage = 1;
						}					
					}, function(res) {
						console.log(res);
					});
				};

				var profileStatus = function() {
					// do we have all required details for user account?
					svrAuth.checkProfileStatus(token, function(res) {
						if (res.data.profilestatus.response.complete === false) {
							var missing = res.data.profilestatus.response.missing;
							$rootScope.traditional.missingFields = missing;
							$rootScope.traditional.token = token;
							$location.url('/signinVerify.html');
						} else {
							// done! tell the app to close this webview
							svrAuth.completeLogin(token, function(res) {
								console.log(res);
								console.log('nice work. now back to the app!');
							}, function(res) {
								console.log(res);
							});
						}
					}, function(res) {
						console.log(res);
					});
				};

			}
	}]);
}());
