var helper = require('./helper.js');

describe('onboarding screens', function() {

	beforeEach(function() {
		browser.get('http://localhost:52000');
	});

	var dirtyTheField = function(elem, index) {
		var logo = element(by.css('.od-logo'));

		elem[index].click();
		logo.click();
		expect(helper.hasClass(elem[index], 'ng-touched')).toBe(true);
		expect(helper.hasClass(elem[index], 'ng-invalid')).toBe(true);
	};

	it('should navigate through the onboarding process', function() {

		var firstPageButton = element(by.css('.dot-one')),
			secondPageButton = element(by.css('.dot-two')),
			thirdPageButton = element(by.css('.dot-three')),
			nextButton = element(by.css('.ob-next')),
			signinButton = element(by.css('.traditional-login'));

		firstPageButton.click();
		expect(firstPageButton.getAttribute('class')).toMatch('active');
		nextButton.click();
		expect(secondPageButton.getAttribute('class')).toMatch('active');
		thirdPageButton.click();
		expect(signinButton.isDisplayed()).toBeTruthy();
	});

	it('should redirect when "Create a new account" link is clicked', function() {
		var createAccountLink = element(by.css('.create-account')),
			thirdPageButton = element(by.css('.dot-three'));

		thirdPageButton.click();
		createAccountLink.click();
		expect(browser.getCurrentUrl()).toMatch(/\/createAccount.html/);
	});

	it('should redirect when "Forgot password" link is clicked', function() {
		var forgotPasswordLink = element(by.css('.forgot-password')),
			thirdPageButton = element(by.css('.dot-three'));

		thirdPageButton.click();
		forgotPasswordLink.click();
		expect(browser.getCurrentUrl()).toMatch(/\/forgotPassword.html/);
	});

	it('should show validation error if focus is lost on error-empty field', function() {
		var thirdPageButton = element(by.css('.dot-three'));

		thirdPageButton.click();

		element.all(by.css('.error-empty')).then(function(elem) {
			for(var i = 0; i < elem.length; i++) {
				dirtyTheField(elem, i);
			}
		});
	});

	it('should activate the "Sign in" button when no fields are left empty', function() {
		var thirdPageButton = element(by.css('.dot-three')),
			email = element(by.css('.email')),
			password = element(by.css('.password')),
			signinButton = element(by.css('.traditional-login'));

		thirdPageButton.click();
		email.sendKeys('test@test.com.au');
		password.sendKeys('Password1');
		expect(signinButton.isEnabled()).toBe(true);
	});
});
