module.exports = {
	hasClass: function(element, cls) {
		return element.getAttribute('class').then(function(classes) {
			return classes.split(' ').indexOf(cls) !== -1;
	 	});
	},
	/*dirtyTheField: function(elem, index) {
		var spacer = element(by.css('.spacer'));

		elem[index].click();
		spacer.click();
		expect(helper.hasClass(elem[index], 'ng-touched')).toBe(true);
		expect(helper.hasClass(elem[index], 'ng-invalid')).toBe(true);
	}*/
};