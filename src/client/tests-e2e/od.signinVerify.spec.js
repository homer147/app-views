var helper = require('./helper.js');

describe('login screen', function() {

	beforeEach(function() {
		browser.get('http://localhost:52000/signinVerify.html');
	});

	var dirtyTheField = function(elem, index) {
		var spacer = element(by.css('.paragraph'));

		elem[index].click();
		spacer.click();
		expect(helper.hasClass(elem[index], 'ng-touched')).toBe(true);
		expect(helper.hasClass(elem[index], 'ng-invalid')).toBe(true);
	};

	it('should show validation error if focus is lost on error-empty field', function() {

		element.all(by.css('.error-empty')).then(function(elem) {
			for(var i = 0; i < elem.length; i++) {
				dirtyTheField(elem, i);
			}
		});
	});

	/*it('should activate the "Confirm" button when no fields are left empty', function() {
		var confirmButton = element(by.css('.confirm')),
			gender = element(by.css('.gender')),
			dob = element(by.css('.dob'));

		element(by.cssContainingText('.dd-list', 'Male')).click();
		dob.sendKeys('0191979');

		expect(confirmButton.isEnabled()).toBe(true);
	});*/

});