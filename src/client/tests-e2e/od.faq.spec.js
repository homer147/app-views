var helper = require('./helper.js');

describe('faq screen', function() {
	beforeEach(function() {
		browser.get('http://localhost:52000/faqs.html');
	});

	it('should open a faq when a title is clicked', function() {
		var title = element.all(by.css('.faq-title')).first(),
			content = title.element(by.xpath('..')).element(by.css('.faq-content'));

		title.click();
		expect(helper.hasClass(content, 'show')).toBe(true);
	});
});