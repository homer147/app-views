var helper = require('./helper.js');

describe('forgot password screen', function() {
	beforeEach(function() {
		browser.get('http://localhost:52000/forgotPassword.html');
	});

	it('shoud show validation error if focus is lost on email field, or if email is invalid', function() {
		var email = element(by.css('.email')),
			copy = element.all(by.css('.fp-copy')).first();

		email.click();
		copy.click();
		expect(helper.hasClass(email, 'ng-touched')).toBe(true);
		expect(helper.hasClass(email, 'ng-invalid')).toBe(true);
		email.sendKeys('ouhoh');
		expect(helper.hasClass(email, 'ng-touched')).toBe(true);
		expect(helper.hasClass(email, 'ng-invalid-email')).toBe(true);
		email.clear();
		email.sendKeys('someone@somewhere.com.au');
		expect(helper.hasClass(email, 'ng-valid')).toBe(true);
	});

	it('should activate the "Reset Password" button when form is valid', function() {
		var email = element(by.css('.email')),
			resetPswrdButton = element(by.css('.reset-password')),
			signinButton = element(by.css('.fp-signin'));

			email.sendKeys('someone@somewhere.com.au');
			expect(resetPswrdButton.isEnabled()).toBe(true);
			resetPswrdButton.click();
			expect($('[ng-show="fpPage===1"].content').isDisplayed()).toBeFalsy();
			expect($('[ng-show="fpPage===2"].content').isDisplayed()).toBeTruthy();
	});

	it('should redirect when "FAQs" link is clicked', function() {
		var faqsLink = element(by.css('.faq-link'));

		faqsLink.click();
		expect(browser.getCurrentUrl()).toMatch(/\/faqs.html/);
	});
});
