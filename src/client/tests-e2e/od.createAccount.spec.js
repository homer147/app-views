var helper = require('./helper.js');

describe('create account screen', function() {
	beforeEach(function() {
		browser.get('http://localhost:52000/createAccount.html');
	});

	var dirtyTheField = function(elem, index) {
		var h3 = element(by.css('.heading'));

		elem[index].click();
		h3.click();
		expect(helper.hasClass(elem[index], 'ng-touched')).toBe(true);
		expect(helper.hasClass(elem[index], 'ng-invalid')).toBe(true);
	};

 	it('should not allow user past the screens while fields are invalid', function(){
 		var caNextButton = element(by.css('.ca-next')),
 			caFirstName = element(by.css('.ca-first-name')),
 			caLastName = element(by.css('.ca-last-name'));

 		caNextButton.click();
 		expect(helper.hasClass(caNextButton, 'ca1')).toBe(true);
 		caFirstName.sendKeys('John');
 		caLastName.sendKeys('Doe');
 		caNextButton.click();
 		expect(helper.hasClass(caNextButton, 'ca2')).toBe(true);
 	});

 	it('should redirect when "FAQs" link is clicked', function() {
		var faqLink = element(by.css('.faq-link'));

		faqLink.click();
		expect(browser.getCurrentUrl()).toMatch(/\/faqs.html/);
	});

	/*it('should show validation error if focus is lost on error-empty field', function() {

		element.all(by.css('.error-empty')).then(function(elem) {
			for(var i = 0; i < elem.length; i++) {
				dirtyTheField(elem, i);
			}
		});
	});

	it('shoud show validation error if focus is lost on email field, or if email is invalid', function() {
		var email = element(by.css('.email')),
			h3 = element(by.css('.heading'));

		email.click();
		h3.click();
		expect(helper.hasClass(email, 'ng-touched')).toBe(true);
		expect(helper.hasClass(email, 'ng-invalid')).toBe(true);
		email.sendKeys('ouhoh');
		expect(helper.hasClass(email, 'ng-touched')).toBe(true);
		expect(helper.hasClass(email, 'ng-invalid-email')).toBe(true);
		email.clear();
		email.sendKeys('someone@somewhere.com.au');
		expect(helper.hasClass(email, 'ng-valid')).toBe(true);
	});

	it('should activate the "Confirm" button when form is valid and redirect when "Confirm" button is clicked', function() {
		var email = element(by.css('.email')),
			confirmButton = element(by.css('.confirm'));

		element.all(by.css('.error-empty')).each(function(elem) {
			elem.sendKeys('test');
		});
		email.sendKeys('someone@somewhere.com.au');
		expect(confirmButton.isEnabled()).toBe(true);
		confirmButton.click();
		expect(browser.getCurrentUrl()).toMatch(/\/signinVerify.html/);
	});*/
});